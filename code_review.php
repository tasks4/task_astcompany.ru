<?php


/**
 *  interface iCart слишком «толстые» интерфейс необходимо разделять на более маленькие и специфические
 *  имя  должен начинатся с большим буквам или можно через префикс писать имя интерфейса CartInterface
 */
interface iCart
{
    /**
     * имя функции должен полностью писать а не calc про это хорошо говорится в книги "Чистый код" Р. Мартин
     * фото https://cloud.mail.ru/public/E8z3/XFKGFFSFE
     * не указана тип функций что должен возвращать
     */
    public function calcVat(): int;

    /**
     * @return void
     */
    public function notify(): void;

    /**
     * @param $discount
     * @return void
     */
    public function makeOrder($discount = 1.0): void;
}

interface NotificationInterface
{
    /**
     * @return void
     */
    public function notify(): void;
}

interface OrderInterface
{
    /**
     * @param float $discount
     * @return void
     */
    public function makeOrder(float $discount = 1.0): void;
}

interface CartInterface
{
    /**
     * @return int
     */
    public function calculateVAT(): int;
}


/**
 * 1) по всем функциям и параметрам нужен писать документация что принимает и что возвращает или какого типа это перемения
 * 2) этот класс нарушенный принципе SOLID (Single responsibility — принцип единственной ответственности) думаю лучше было разделить на 3 части
 *  для расчета, для отправки email или notify и для созданный заказов
 *
 */
class Cart implements CartInterface, NotificationInterface, OrderInterface
{
    /**
     * Думаю доступы здесь должно private быть и реализовать getter setter
     *
     * @var
     */
    public $items;

    /**
     *
     * Думаю доступы здесь должно private быть  и реализовать getter setter
     *
     * @var
     */
    public $order;


    public function calcVat()
    {
        $vat = 0;

        // пробел по  PSR12 стандартом
        foreach ($this->items as $item) {
            // 0.18 что за цифр ? нужен было это писать верху класса и по константу что понятно было за что ответить это цифр
            // и если дальнейшем нужен будет изменить смогли с одной месте все изменить

            $vat += $item->getPrice() * 0.18;
        }

        //пробел по PSR12 стандартом
        return $vat;
    }

    /**
     * @return int
     */
    public function calculateVAT(): int
    {
        $vat = 0;

        /**
         *  я здесь места foreach попитался проблему решить по нативной функций php -  array_column(); array_sum()
         */

        foreach ($this->items as $item) {
            $vat += $item->getPrice() * 0.18;
        }

        return $vat;
    }

    /**
     * @return void
     */
    public function notify(): void
    {
        $this->sendMail();
    }

    /**
     * по названный переменный сверху писал что должно по смыслу быть
     *
     * это функция можно реализовать по dependency injection правилам чтобы не  зависом было от класса SimpleMailer
     * который принимает $mailer параметр с типом MailerInterface (MailerInterface $mailer)
     *
     * @return void
     */
    public function sendMail()
    {
        /**
         * Названия объекта лучше назвать как названия класса
         * Передованый параметры для красоты можно оделенной переменой писать чтобы ясно было какое значения там например
         *
         * $username = 'cartuser';
         * $password = 'j049lj-01'';
         *
         */

//        $m = new SimpleMailer('cartuser', 'j049lj-01');
        $simpleMailer = new SimpleMailer('cartuser', 'j049lj-01');

        /**
         * место этого кода можно вызывать метода calculateVAT()
         */

//        $p = 0;
//
//        foreach ($this->items as $item) {
//            $p += $item->getPrice() * 1.18;
//        }

//        $vat = $this->calculateVAT();

        /**
         * названия пермения должен быть messages и вместе конкатенаций лучше использовать sprintf() функция
         */

//        $ms = "<p> <b>" . $this->order->id() . "</b> " . $p . " .</p>";
//        $messages=
//            sprintf(
//                "<p> <b>%s</b> %s .</p>",
//                $this->order->id(),
//                $this->calculateVAT()
//            );

        $simpleMailer->sendToManagers(
            sprintf(
                "<p> <b>%s</b> %s .</p>",
                $this->order->id(),
                $this->calculateVAT()
            )
        );
    }


    /**
     * @param float $discount
     * @return void
     */
    public function makeOrder(float $discount = 1.0): void
    {
        /**
         * 1 опять название переменной должен быть по смыслу
         * 2 я здес места foreach попитался проблему решить по нативной функцый php -  array_column(); array_sum()
         *
         * 3 или вызывать метод calculateVAT() * $discount
         *
         */

        $p = 0;

        foreach ($this->items as $item) {
            $p += $item->getPrice() * 1.18 * $discount;
        }

        $this->order = new Order($this->items, $p);

        $this->sendMail();
    }
}